package simple

import (
	"database/sql"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var (
	db       *gorm.DB
	sqlDB    *sql.DB
	dbconfig *gorm.Config
)

func OpenDB(DbType string, dsn string, TablePrefix string, config *gorm.Config, maxIdleConns, maxOpenConns int, models ...interface{}) (err error) {
	if config == nil {
		config = &gorm.Config{}
	}
	dbconfig = config
	if config.NamingStrategy == nil {
		config.NamingStrategy = schema.NamingStrategy{
			TablePrefix:   TablePrefix,
			SingularTable: true,
		}
	}
	var dberr error
	if DbType == "mssql" {
		db, dberr = gorm.Open(sqlserver.Open(dsn), config)
	} else if DbType == "mysql" {
		db, err = gorm.Open(mysql.Open(dsn), config)
	}

	if dberr != nil {
		return dberr
	}

	if sqlDB, err = db.DB(); err == nil {
		sqlDB.SetMaxIdleConns(maxIdleConns)
		sqlDB.SetMaxOpenConns(maxOpenConns)
		sqlDB.SetConnMaxLifetime(time.Hour)
	} else {
		return err
	}

	for _, model := range models {
		if !db.Migrator().HasTable(model) {
			if err = db.AutoMigrate(model); nil != err {

			}
		}
	}
	return
}

// DB 获取数据库链接
func DB() *gorm.DB {
	return db
}
func Prefix() string {
	return "tmaic_"
}

// CloseDB 关闭连接
func CloseDB() {
	if sqlDB == nil {
		return
	}
	if err := sqlDB.Close(); nil != err {

	}
}
